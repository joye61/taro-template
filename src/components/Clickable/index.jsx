import style from "./index.module.scss";
import { View } from "@tarojs/components";

export function Clickable(props) {
  const {
    children,
    hoverClass = style.hover,
    hoverStartTime = 0,
    hoverStayTime = 50,
    ...extra
  } = props;

  return (
    <View
      hoverClass={hoverClass}
      hoverStartTime={hoverStartTime}
      hoverStayTime={hoverStayTime}
      {...extra}
    >
      {children}
    </View>
  );
}
