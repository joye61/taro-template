import { View } from "@tarojs/components";
import style from "./index.module.scss";
import clsx from "clsx";
import { Clickable } from "../Clickable";

export function Flex(props) {
  const { children, clickable = false, ...extra } = props;
  const BoxComponent = clickable ? Clickable : View;
  return <BoxComponent {...extra}>{children}</BoxComponent>;
}

export function RowStart(props) {
  const { className, ...extra } = props;
  return (
    <Flex className={clsx(style.row, style.start, className)} {...extra} />
  );
}
export function RowEnd(props) {
  const { className, ...extra } = props;
  return <Flex className={clsx(style.row, style.end, className)} {...extra} />;
}
export function RowBetween(props) {
  const { className, ...extra } = props;
  return (
    <Flex className={clsx(style.row, style.between, className)} {...extra} />
  );
}
export function RowAround(props) {
  const { className, ...extra } = props;
  return (
    <Flex className={clsx(style.row, style.around, className)} {...extra} />
  );
}
export function RowCenter(props) {
  const { className, ...extra } = props;
  return (
    <Flex className={clsx(style.row, style.center, className)} {...extra} />
  );
}
export function RowEvenly(props) {
  const { className, ...extra } = props;
  return (
    <Flex className={clsx(style.row, style.evenly, className)} {...extra} />
  );
}

export function ColStart(props) {
  const { className, ...extra } = props;
  return (
    <Flex className={clsx(style.col, style.start, className)} {...extra} />
  );
}
export function ColEnd(props) {
  const { className, ...extra } = props;
  return <Flex className={clsx(style.col, style.end, className)} {...extra} />;
}
export function ColBetween(props) {
  const { className, ...extra } = props;
  return (
    <Flex className={clsx(style.col, style.between, className)} {...extra} />
  );
}
export function ColAround(props) {
  const { className, ...extra } = props;
  return (
    <Flex className={clsx(style.col, style.around, className)} {...extra} />
  );
}
export function ColCenter(props) {
  const { className, ...extra } = props;
  return (
    <Flex className={clsx(style.col, style.center, className)} {...extra} />
  );
}
export function ColEvenly(props) {
  const { className, ...extra } = props;
  return (
    <Flex className={clsx(style.col, style.evenly, className)} {...extra} />
  );
}
