import style from "./index.module.scss";
import { View } from "@tarojs/components";
import clsx from "clsx";

export function SafeArea(props) {
  const { type = "bottom", className, ...extra } = props;
  let saClass = undefined;
  if (type === "top") {
    saClass = style.top;
  } else if (type === "bottom") {
    saClass = style.bottom;
  }
  return <View className={clsx(saClass, className)} {...extra} />;
}
