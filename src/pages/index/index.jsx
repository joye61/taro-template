import { View , Image} from "@tarojs/components";
import {
  RowAround,
  RowBetween,
  RowCenter,
  RowEnd,
  RowEvenly,
  RowStart,
  ColCenter,
} from "../../components/Flex";
import { Dialog } from "../../components/Dialog";
import { useState } from "react";
import { SafeArea } from "../../components/SafeArea";

function TBox() {
  return (
    <View
      style={{ width: "100rpx", height: "100rpx", backgroundColor: "#f0f0f0" }}
    />
  );
}

export default () => {
  const [show, setShow] = useState(false);

  return (
    <>
      <Image src={require("../../assets/box.svg")} style={{width: "100rpx"}} mode="widthFix" />
      <RowAround>
        <TBox />
        <TBox />
      </RowAround>
      <RowBetween
        clickable
        onClick={() => {
          setShow(true);
        }}
      >
        <TBox />
        <TBox />
      </RowBetween>
      <RowStart>
        <TBox />
        <TBox />
      </RowStart>
      <RowEnd>
        <TBox />
        <TBox />
      </RowEnd>
      <RowCenter>
        <TBox />
        <TBox />
      </RowCenter>
      <RowEvenly>
        <TBox />
        <TBox />
      </RowEvenly>

      <Dialog
        show={show}
        type="pullUp"
        onBlankClick={() => {
          setShow(false);
        }}
      >
        <ColCenter style={{ backgroundColor: "#fff" }}>
          hello world
          <SafeArea />
        </ColCenter>
      </Dialog>
    </>
  );
};
